using System;
using System.Collections.Generic;
using System.Text;

namespace Iterator
{
    class ListaPalabrasIterador:Iterador // aqui se implementa la interfaz Iterador
    {
        string palabra1, palabra2, palabra3, palabra4;
        int posicion;
        public ListaPalabrasIterador(string p1, string p2, string p3, string p4) // con este constructor damos valor inicial a los atributos
        {
            this.palabra1 = p1;
            this.palabra1 = p2;
            this.palabra1 = p3;
            this.palabra1 = p4;
            this.posicion = 0;

        }
        public object siguiente()
        {
            switch (posicion++) // dependiendo de la posición se retornará una palabra
            {
                case 0:
                   return palabra1;
                    
                case 1:
                    return palabra2;
                   
                case 2:
                    return palabra3;
                   
                case 3:
                  return  palabra4;
                
            }
            return null;
        }
        public Boolean tieneSiguiente() // se retorna un booleano si el array aun tiene elementos
        {
            if (posicion <4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
