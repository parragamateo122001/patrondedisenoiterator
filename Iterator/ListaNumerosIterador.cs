using System;
using System.Collections.Generic;
using System.Text;

namespace Iterator
{
    class ListaNumerosIterador : Iterador
    { 
//se declaró un array de tipo int y un atributo del mismo tipo. El array es para los elementos que va a recorrer el iterador y el atributo posición para indicar si aun hay elementos en el array
        private int[] numeros; 
        private int posicion;

        public ListaNumerosIterador(int [] num) // aqui recibe el array de la clase lista_numeros
        {
            this.numeros = num;     // al array numeros se le asigna el array que recibe como parametro en su método
            this.posicion = 0;
        }
        public  object siguiente() // cuando se llame a este método este retornara el número de la posición que se tiene
        {
            return numeros[posicion++]; 


       // cuando se llame a este método desde la clase PruebaIterador se retornará un valor booleano que indica si aun hay valores en el array
        }public  Boolean tieneSiguiente() 
        {
           if (posicion <numeros.Length && numeros[posicion] != 0)
          {
                return true;
          }else
          {
                return false;
          }
        }
    }
}
