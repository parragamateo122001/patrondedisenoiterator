using System;
using System.Collections.Generic;
using System.Text;

namespace Iterator
{
    class PruebaIterador
    {
        public static void Main (string[]args)
        {
            Lista_numeros ln = new Lista_numeros(); // se creo un objeto de tipo Lista_numeros para acceder a metodos y propiedades de esta clase
            Lista_palabras lp = new Lista_palabras();// se creo un objeto de tipo Lista_palabras para acceder a metodos y propiedades de esta clase
            Iterador iterador; // se declaro un objeto de tipo Iterator que puede acceder a las clases que implementan la interfaz de este nombre
            ln.agregar(5);
            ln.agregar(2);
            ln.agregar(15);
            ln.agregar(23);

            lp.agregar("dos");
            lp.agregar("tres");
            lp.agregar("nueve");
            lp.agregar("nueve");
            iterador = ln.iterador();
            //  "iterador" se convierte en un objeto, puesto que, ln.iterator(); retorna lo sigiente: new ListaPalabrasIterator (numeros);
            while (iterador.tieneSiguiente()) // mientras esto se cumpla se iran ingresando uno a uno los numeros
            {
            int numero = (int)iterador.siguiente(); // A "numero" se le asigna lo que retorna iterator.siguiente(); y se transforma a int 
                Console.WriteLine(numero);// se van imprimiendo los numeros miestras continue el ciclo
            }
}
    }
}
        
