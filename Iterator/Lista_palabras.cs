using System;
using System.Collections.Generic;
using System.Text;

namespace Iterator
{
    class Lista_palabras
    {
        // aquí se declaran los atributos palabra1, palabra2, palabra3, palabra4 y posicion para indicar si hay numeros por recorrer
        string palabra1, palabra2, palabra3, palabra4;
        int posicion;
        public Lista_palabras() // Este constructor se creó para dar un valor de arranque al atributo posición
        {
            posicion = 0;
        }
        public void agregar(string p) 
        // cuando se llama a este método desde otra clase se le envia un valor que se asigna al atributo dependiendo del valor del switch 
        {
            switch (posicion)
            {
                case 0:
                    palabra1 = p;
                    break;
                case 1:
                    palabra2 = p;
                    break;
                case 2:
                    palabra3 = p;
                    break;
                case 3:
                    palabra4 = p;
                    break;
            }
            posicion++;
            }
        // este método retorna una instancia e invoca al constructor de la clase ListaPalabrasIterador y le pasa los atributos 
        public ListaPalabrasIterador iterador ()
        {
            return new ListaPalabrasIterador(palabra1, palabra2, palabra3, palabra4);

            // cuando se crea el objeto, automaticamente se invoca al costructor y se le pasa el argumento

        }
    }
}



