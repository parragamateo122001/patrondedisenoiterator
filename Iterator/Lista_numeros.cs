using System;

namespace Iterator
{
    class Lista_numeros
    {
        int[] numeros; // aquí se declaran dos atributos, el array para almacenar los elementos y posicion para indicar si hay numeros por recorrer
        int posicion;

        public Lista_numeros()//este constructor se creó para dar tamaño al array y para asignarle un valor de cero al atributo posición 
        {
            numeros = new int[10];
            posicion = 0;
        }
        public void agregar(int i)
        {
            numeros[posicion++] = i; // "numeros" recibe el valor que se pasa por parametro al llamar al método agregar y luego se incrementa en uno
        }
        public ListaNumerosIterador iterador() // cuando se llama a este método se retorna una istancia de la clase ListaNumerosIterador
        {
            return new ListaNumerosIterador(numeros); //cuando se crea el objeto, automaticamente se invoca al costructor y se le pasa el argumento
        }

}
}


