using System;
using System.Collections.Generic;
using System.Text;

namespace Iterator
{
    interface Iterador
    {
        // los metodos de una interfaz en c sharp no llevan modificador de acceso
         object siguiente(); // se creo este método que devuelve un objeto
         Boolean tieneSiguiente(); //se creo este método que devuelve un booleano

         //Los métodos de esta interfaz seran implementados en las clases ListaNumerosIterador y ListaPalabrasIterador
    }
}
